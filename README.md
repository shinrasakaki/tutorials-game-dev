# CSCE604121 Game Development - Tutorials

> Koleksi materi tutorial perancangan game dan penggunaan game engine.

## Lisensi

Copyright (c) 2021 Tim Pengajar Kuliah Game Development Fakultas Ilmu
Komputer Universitas Indonesia.

Lisensi untuk menggunakan, menggubah, memperbaiki, dan membuat ciptaan turunan
dari koleksi materi tutorial ini diatur dalam dua lisensi: [Creative Commons Attribution Share-Alike 4.0 (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/)
dan [MIT](https://opensource.org/licenses/MIT). Lisensi CC BY-SA 4.0 berlaku
terhadap materi instruksional dari tutorial seperti dokumen dan gambar.
Sedangkan lisensi MIT berlaku untuk kode sumber yang menjadi bagian dari materi
tutorial.
